// console.log("Hello World!")


// Objective 1
// console.log(fetch("https://jsonplaceholder.typicode.com/todos").then(response => console.log(response.status)));
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
})

// Objective 2

fetch ("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => {
    console.log(json);
    console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
    
});


// Objective 3

fetch ("https://jsonplaceholder.typicode.com/todos", {
       method: "POST" ,
       headers: {
        'Content-Type': 'application/json'
       },
       body: JSON.stringify({
        completed: false,
        id: 201,
        title: "Created To Do List Item",
        userId: 1
       })    

})
.then(response => response.json())
.then(json => console.log(json))

// Objective 4

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        id: 1,
        status: "Pending",
        title: "Updated To Do List Item",
        userId: 1
    })

})
.then(response => response.json())
.then(json => console.log(json))

// Objective 5

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        status: "complete"
    })

})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/201", {
    method: "DELETE"
})
